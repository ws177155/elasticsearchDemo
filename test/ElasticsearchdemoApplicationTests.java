package com.example.elasticsearchdemo;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Iterator;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ElasticsearchdemoApplicationTests {

    @Autowired
    ArticleRepository articleRepository;

    @Test
    public void contextLoads() {
    }

    @Test
    public void save() {
        ItemDocument itemDocument = new ItemDocument();
        itemDocument.setCatId(5);
        itemDocument.setDescription("这可真是啊棒棒");
        itemDocument.setId(4L);
        itemDocument.setName("啊3凯");
        ItemDocument save = articleRepository.save(itemDocument);
        System.out.println(save);
    }

    @Test
    public void search() {
        Optional<ItemDocument> byId = articleRepository.findById(2L);
        ItemDocument itemDocument = byId.get();
        System.out.println(itemDocument.toString());
    }

    @Test
    public void searchIK() {

        QueryBuilder query =  QueryBuilders.matchQuery("name", "'啊凯");
        Iterable<ItemDocument> search = articleRepository.search(query);
        Iterator<ItemDocument> iterator = search.iterator();
        while (iterator.hasNext()) {
            ItemDocument next = iterator.next();
            System.out.println(next.toString());
        }
        System.out.println(search.toString());
    }

}
